# Apps I use on Mac

## Apps for Development

### Backend Development:

In my profesional life I'm a Python dev so I use PyCharm on a daily basis, but I was previously a TypeScript/NodeJS developer so some of the tools I use were influenced by my previous experience.

- PyCharm: My IDE for editing Python code. If I am working with a different language I use VSCode
- VSCode: If I'm writing a backend or API in anything other than Python I use VSCode to edit my code.
- Postman: I use Postman to test my APIs to make sure that requests are accepting the correct types and sending errors and responses correctly. The features are very great, you can use JavaScript in Postman to set variables based on responses from specific requests and reuse them across a set context within Postman.
- Postico: An application that runs natively on macOS that allows you to go through tables in a database and see how they are structured, what the current values in a table are, execute mock database queries to make sure that data you are putting into the database is what you are expecting or that your query is setup correctly.

### Frontend Development:

I don't do a lot of frontend development (yet) so I don't know a lot of good tools to help assist in frontend development so if you have any suggestions, please submit a PR adding to this list with the tool and why you use it and I'll give it a look over it and merge it in if I like it.

- VSCode: Currently it's my editor/IDE of choice for frontend/JS/TS development as it's an Electron app so the support it comes with for JS/TS/frontend development is great and with all the extensions it makes it even better.
- Firefox DevTools: I know that almost all frontend devs use Chrome DevTools because they seem to be an industry standard, but I find Firefox's DevTools to be significantly better than Chrome's. The only thing that Chrome has going for it over Firefox is that you can customize the theme of the DevTools.

## Communication Apps

- Discord
- Slack
- Signal
- Keybase
- WhatsApp

## Miscellaneous

- Spotify: One of the most popular music services in the world today. You can listen to a variety of artists for free (with ads after a random number of songs), or you can pay a very good price for Premium which allows you listen to music ad-free, download songs to listen offline, and a variety of other features.
- Firefox: The default browser on most Linux distributions (it's been the default on all distributions I've tried and the distributions that I currently use). It is one of the better browsers for privacy and security and has all the features Chrome has without the propreitary software behind it. Firefox is an open source browser that is continuously getting better and is one of my go to browsers for everyday browsing. It's the only browser I have on my work computer.
- Brave Browser: The most private browser by default. Brave ships with an adblocker on by default that allows you to customize what you block. It is also changing how users interact with websites and content creators by having an ad system built into it that sends you a notification of an available ad and allows you to choose whether you want to view the ad or not, regardless of whether or not you choose to view the ad, you are reward with an Ethereum ERC-20 token called BAT (Basic Attention Token), which you can then use to tip content creators that you like or set up monthly payments that split a set amount of BAT and sends it to sites you viewed based on how much attention you gave them. Sites like DuckDuckGo, Wikipedia, and The Washington Post now accept BAT. Because of the rise in popularity of Brave and BAT some websites (like The Washington Post) are experimenting with models where you can setup monthly contributions of BAT to their website in order to get access to all their content completely ad-free, that means all you have to do is use Brave with Brave Rewards setup, set how many ads you are willing to view in an hour and then you can pay to get news articles from The Guardian or The Washington Post just by using the internet. If you want to start using Brave you can download it [here](https://brave.com/bie930).
- Chrome: While I'm starting to move away from using Google services I am only using Chrome for Google services. I only do minimal web browsing with Chrome and if I do I use DuckDuckGo instead of Google.
- Boostnote: A note taking app made by developers for developers. You can choose between to types of notes, Markdown notes or Snippet notes. Markdown notes are just notes that you write using Markdown to help format your notes for you to look at later that also allows you to put code snippets into your notes that makes them look different from other parts of your notes. Snippet notes are just sample code notes, they are useful if you are in a meeting and need to prototype something you can write sample code for you to review later. Snippets support a significant number of languages including all the major ones like Python, JavaScript, PHP, Go, CSS, HTML, Java, and C++.
- Tor: The privacy and security nuts browser. I don't really use it all that often as I don't too much internet browsing that I want to hide from anyone and watching anime with the players that the websites I use would defeat the purpose of using Tor. It is interesting to go look at different `.onion` websites occasionally.
- Speedtest: An app that allows me to test the speed of my internet connection. It is created by the creators of speedtest.net, so it uses their service just from a standalone app. It's helpful when I'm trying to figure out if my connection problems are with my browser/extensions or my ISP.
- coconutBattery: Shows information about the health of your MacBook's battery.
- ProtonVPN: A VPN service provided by the makers of ProtonMail that you can use for free or bundle with your ProtonMail subscription.

### Gaming/Gaming peripherals

- Steam: Steam is one of the biggest video game distribution services in the world and are one of the biggest proponents of making all video games run natively on Linux. Their product, ProtonDB, is allowing developers to create games and ship them to Steam and allow them to run on Linux without making game developers spend extra time on the game. A significant portion of my games are on Steam.
2) Spotify
3) Discord
4) WhatsApp
5) Signal
6) Google Chrome
7) Firefox
8) Coconut Battery
9) Speedtest
10) ProtonVPN
11) Iterm2
12) VSCode
13) Postman
14) Insomnia
15) Postgres.app
16) Postico
17) 1password
18) Steam
